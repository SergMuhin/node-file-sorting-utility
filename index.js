const fs = require('fs').promises;
const path = require('path');

async function readDir(base) {
	const filesPathsArr = [];
	const foldersArr = [];

	async function readDirectory(base, level) {
		const files = await fs.readdir(base);
		for (const item of files) {
			const localBase = path.join(base, item);
			const state = await fs.stat(localBase);

			filesPathsArr.push(localBase);

			if (state.isDirectory()) {
				foldersArr.push(localBase);
				await readDirectory(localBase, level + 1);
			}
		}
	}

	await readDirectory(base, 0);

	return { filesPathsArr, foldersArr };
}

async function createFiles(filesPathsArr, foldersArr, extensions, deleteBase, newPath) {
	const allowedExtensions = extensions || 'all';

	for (const fileBasePath of filesPathsArr) {
		const file = path.parse(fileBasePath).base;
		const fileExtension = path.extname(fileBasePath).substr(1);
		const state = await fs.stat(fileBasePath);

		if (state.isFile() && (allowedExtensions === 'all' || allowedExtensions.includes(fileExtension))) {
			const folder = file.substr(0, 1).toUpperCase();
			const fileFolder = path.join(newPath, folder);

			try {
				await fs.mkdir(fileFolder, { recursive: true });
				await fs.copyFile(fileBasePath, path.join(fileFolder, file));
				if (deleteBase === 'delete') {
					await fs.unlink(fileBasePath);
				}
			} catch (error) {
				console.error(error);
			}
		}
	}

	if (deleteBase === 'delete' && allowedExtensions === 'all') {
		for (const emptyFolder of foldersArr) {
			try {
				await fs.rmdir(emptyFolder);
			} catch (error) {
				console.error(error);
			}
		}
		try {
			await fs.rmdir(path.dirname(base));
		} catch (error) {
			console.error(error);
		}
	}
}

async function main() {
	try {
		const base = process.argv[2];
		const newPath = path.join(path.parse(base).dir, 'Your sorted folder');
		await fs.mkdir(newPath, { recursive: true });

		const { filesPathsArr, foldersArr } = await readDir(base);

		const extensions = process.argv[3];
		const deleteBase = process.argv[process.argv[4] === 5 ? 4 : 3];
		await createFiles(filesPathsArr, foldersArr, extensions, deleteBase, newPath);
	} catch (error) {
		console.error(error);
	}
}

main();
