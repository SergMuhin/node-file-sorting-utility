
# Node File Sorter

This is a command-line utility built with Node.js that sorts files within a directory based on their names.

## Description

Node File Sorter is a simple tool designed to organize files within a directory by sorting them alphabetically according to their filenames. It scans a specified directory, identifies all files, and moves them to folders named after the first letter of their filename. For example, files starting with "A" are moved to a folder named "A", files starting with "B" are moved to a folder named "B", and so on. This helps in organizing large collections of files and makes it easier to locate specific files based on their names.

## How to Use

1. **Installation**: Clone the repository or download the source code to your local machine.

2. **Usage**: Run the utility from the command line with the following command:

   ```bash
   node index.js <directory_path>
   ```

   Replace `<directory_path>` with the path to the directory containing the files you want to sort.

3. **Result**: After execution, the utility will organize the files within the specified directory by moving them into subdirectories named after the first letter of their filenames.

## Example

Suppose you have a directory named "files" with the following files:

```
apple.txt
banana.txt
carrot.txt
grape.txt
```

After running the utility on the "files" directory, the structure will be as follows:

```
- files
  - a
    - apple.txt
  - b
    - banana.txt
  - c
    - carrot.txt
  - g
    - grape.txt
```
